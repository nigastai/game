// create a new scene named "Game"
let gameScene = new Phaser.Scene('Game');


// some parameters for our scene
gameScene.init = function() {

}

// load asset files for our game
gameScene.preload = function() {
  //Images here

};

// executed once, after assets were loaded
gameScene.create = function() {
  //Remember that sprites are createn on top of each other
  
};

// executed on every frame (60 times per second)
gameScene.update = function() {
  //Here is were all your game logic and animations goes

};
ß
gameScene.gameOver = function() {

};

gameScene.gameWin = function() {

};



// our game's configuration
let config = {
  type: Phaser.AUTO,
  width: 640,
  height: 360,
  scene: gameScene
};

// create the game, and pass it the configuration
let game = new Phaser.Game(config);
